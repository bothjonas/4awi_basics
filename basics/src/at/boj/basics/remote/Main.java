package at.boj.basics.remote;

public abstract class Main {

	public static void main(String[] args) {
	
		Battery b1 = new Battery(60);
		
		remote r1 = new remote(b1);
		
		r1.turnOn();
		
		
		System.out.println(r1.getPowerStatus());
		System.out.println(r1.isOn());
	
	}

}
