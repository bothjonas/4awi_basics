package at.boj.basics.remote;

public class remote 
{
	private boolean isOn;
	private boolean haspower;
	private Battery battery;



	
	public remote(Battery battery) 
	{
		super();
		this.battery = battery;
	}
	
	public remote(boolean isOn, boolean haspower, Battery battery) 
	{
		super();
		this.isOn = isOn = false;
		this.haspower = haspower =true;
		this.battery = battery;
	}
	
	public void turnOn() 
	{
		this.isOn = true;
		System.out.println("Turned on");
	}
	
	public void turnOff()
	{
		this.isOn = false;
		System.out.println("Turned off");
	}
	
	public boolean isOn()
	{
		return this.isOn;
	}
	
	public boolean getPowerStatus()
	{
		if(battery.getPowerStatus() > 50)
		{
			this.haspower = true;
		}
		else
		{
			this.haspower = false;
		}
		return haspower;
	}
}
