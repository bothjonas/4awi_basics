package at.boj.basics.car;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private List<Car> cars;
	
	public Person()
	{
		this.cars = new ArrayList<>();
	}
	
	public void addCar(Car car)
	{
		this.cars.add(car);
	}
	
	public void printYourCars()
	{
		int price = 0;
		for(Car car : cars)
		{
			System.out.println(car.getcarFullName());
		}
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
}
