package at.boj.basics.car;

public class Producer {
	private String producerProducer;
	private String producerLand; //Herstellerland
	private float producerDiscount; //percent
	
	public String getProducerProducer() {
		return producerProducer;
	}
	public void setProducerProducer(String producerProducer) {
		this.producerProducer = producerProducer;
	}
	public String getProducerLand() {
		return producerLand;
	}
	public void setProducerLand(String producerLand) {
		this.producerLand = producerLand;
	}
	public float getProducerDiscount() {
		return producerDiscount;
	}
	public void setProducerDiscount(float producerDiscount) {
		this.producerDiscount = producerDiscount;
	}
	
}
