package at.boj.basics.car;

public class Main {

	public static void main(String[] args) {


		Producer pro1 = new Producer();
		Engine eng1 = new Engine();
		Person per1 = new Person();
		Car car1 = new Car();
		
		car1.setProducer(pro1);
		car1.setCarColour("red");
		car1.setCarKmstand(60000);//km
		car1.setCarMaxvel(240);//km/h
		car1.setCarName("A5");
		car1.setCarPrice(30000);//�
		car1.setCarSeats(5);
		car1.setCarUsage(8);//l/100km
		pro1.setProducerDiscount(5);//%
		pro1.setProducerLand("Germany");
		pro1.setProducerProducer("Audi");
		eng1.setEnginePower(200);//PS
		eng1.setEngineTyp("Diesel");//Diesel oder Benzin
		
		System.out.println("The cars colour is " + car1.getCarColour());
		System.out.println("The car can drive " + car1.getCarMaxvel() + "km/h");
		if(car1.getCarKmstand()>50000)
		{
			car1.setCarUsage((car1.getCarUsage() * 1.098f));
		}
		else
		{	
		}
		System.out.println("The car uses " + car1.getCarUsage() + "l/100km");
		System.out.println("The car was driven " + car1.getCarKmstand() + "km before");
		car1.setCarPrice(car1.getCarPrice() - (car1.getCarPrice()*pro1.getProducerDiscount()/100));
		System.out.println("The car costs " + car1.getCarPrice() + "�");
		System.out.println("The producer is " + pro1.getProducerProducer());
		System.out.println("The producer comes from " + pro1.getProducerLand());
		System.out.println("The produces has a discount of " + pro1.getProducerDiscount() + "% (included in price)");
		System.out.println("The engine is a " + eng1.getEngineTyp() + " type");
		System.out.println("The engine has a power of " + eng1.getEnginePower() + "PS");
		System.out.println("The car has " + car1.getCarSeats() + " seats");
		System.out.println(car1.getcarFullName());
	}
	
}
