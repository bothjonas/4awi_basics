package at.boj.basics.car;

public class Engine {
	public String getEngineTyp() {
		return engineTyp;
	}
	public void setEngineTyp(String engineTyp) {
		this.engineTyp = engineTyp;
	}
	public int getEnginePower() {
		return enginePower;
	}
	public void setEnginePower(int enginePower) {
		this.enginePower = enginePower;
	}
	private String engineTyp; //Diesel/Benzin
	private int enginePower; //PS
}
