package at.boj.basics.car;

public class Car {
	private String carColour;
	private String carName;
	private String producerName;
	private float carPrice; //�
	private float carMaxvel; //km/h
	private float carUsage;//l/100km
	private float carKmstand;
	private int carSeats;
	private String carFullName;
	private Producer producer;


	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public String getcarFullName()
	{
	
	return producer.getProducerProducer() + " " + carName;
	
	}
	
	public String getCarFullName() {
		return carFullName;
	}

	public void setCarFullName(String carFullName) {
		this.carFullName = carFullName;
	}

	public int getCarSeats() {
		return carSeats;
	}




	public void setCarSeats(int carSeats) {
		this.carSeats = carSeats;
	}




	public String getCarColour() {
		return carColour;
	}




	public String getCarName() {
		return carName;
	}




	public void setCarName(String carName) {
		this.carName = carName;
	}




	public float getCarKmstand() {
		return carKmstand;
	}




	public void setCarKmstand(float carKmstand) {
		this.carKmstand = carKmstand;
	}




	public void setCarColour(String carColour) {
		this.carColour = carColour;
	}




	public float getCarPrice() {
		return carPrice;
	}




	public void setCarPrice(float carPrice) {
		this.carPrice = carPrice;
	}




	public float getCarMaxvel() {
		return carMaxvel;
	}




	public void setCarMaxvel(float carMaxvel) {
		this.carMaxvel = carMaxvel;
	}




	public float getCarUsage() {
		return carUsage;
	}




	public void setCarUsage(float carUsage) {
		this.carUsage = carUsage;
	}

	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}






}

